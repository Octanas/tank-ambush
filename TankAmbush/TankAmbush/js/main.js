﻿/////// HTML ELEMENTS 

var messagePause = document.getElementById("messagePause"); // Pause menu
var pause = false;

var messageGameOver = document.getElementById("gameOver");  // Game over text
var gameOver = false;

var menu = document.getElementById("menu"); // main menu
menu.style.display = '';    // Shows menu
var menuON = true;

var buttons = document.getElementById("buttons");   // Buttons from the main menu

var container = document.getElementById("container");   // Container where the scenario will be rendered

// Container for the time
var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;   // Seconds elapsed

var stats = document.getElementById("stats");
stats.style.display = "none";

var hp1 = document.getElementById("hp1");
var hp2 = document.getElementById("hp2");
var hp3 = document.getElementById("hp3");

var Kills = document.getElementById("kills");
var KillCounter = 0;

////// SCENE COMPONENTS

var SPAWN_DISTANCE = 2000;
var MIN_DISTANCE = 200;
var SHOOT_POWER = 800;

var MAX_ENEMIES = 8;
var numberEnemies = 0;

var MAP_WIDTH = 5000;
var MAP_HEIGHT = 5000;

var FOG_DENSITY = 0.001;

var GRAVITY = 400;

var ground;

var tankTemplate = null;
var playerTank;
var hpCounter = 4;
var enemyTanks = [];

var playerBullet = null;

// explode animation variables
var totalParts = 700;
var dirs = [];
var parts = [];
var partsSpeed = 30;
var partsSize = 5;
var partsColors = [0xff5000, 0xff9400, 0xff0000, 0x996600, 0xffd454];

// dismantled parts
var dismantledParts = [];

////// TECHNICAL STUFF

var yrotation = 0.01;
var xrotation = 0.01;
var yisrotatingleft = false;
var yisrotatingright = false;
var xisrotatingup = false;
var xisrotatingdown = false;

var camera, controls, scene, renderer;
var controlsEnabled = false;

var manager = new THREE.LoadingManager();   // Loading manager to load external components

Physijs.scripts.worker = 'js/libs/physijs_worker.js';
Physijs.scripts.ammo = 'ammo_old.js';

// When everything is finished loading, this function will execute
manager.onLoad = function () {

    menuSelection();
    init();
}

var texture;
var textureLoader = new THREE.TextureLoader(manager);
textureLoader.load("./images/Grass_001_COLOR (1).jpg", function (textu) {
    texture = textu;
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(32, 32);
});

// ############################################################################################

function init() {
    scene = new Physijs.Scene({ reportsize: 120, fixedTimeStep: 1 / 60 });
    scene.setGravity(new THREE.Vector3(0, -GRAVITY, 0));

    // Rendering settings
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0x999999);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    container.appendChild(renderer.domElement).style.display = "block"; // Connects the renderer to the HTML container

    // Defines camera position
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
    camera.position.z = 300;
    camera.position.y = 70;

    controls = new THREE.PointerLockControls(camera);   // Adds the camera to the controls
    scene.add(controls.getObject());

    addGround();

    addFog();

    addSun();

    addPlayer();

    addEnemyTank(true, 2, 2000);

    addLights();    // Adds lights to the scene

    window.addEventListener('resize', onWindowResize, false)    // Fixes the canvas when the window is resized

    animate();
}

function buildTank(mass) {
    // create single object

    //----------TANK------------

    var bodyMat = Physijs.createMaterial(new THREE.MeshPhongMaterial({ color: 'green' }, .8, .3));
    var body = new Physijs.BoxMesh(
        new THREE.BoxGeometry(70, 25, 90),
        bodyMat,
        mass
    );

    body.castShadow = true;
    body.receiveShadow = true;

    var neckGeo = new THREE.CylinderGeometry(25, 25, 30, 12);
    var neck = new Physijs.CylinderMesh(neckGeo, bodyMat);


    var headGeo = new THREE.SphereGeometry(25, 12, 12);
    var head = new Physijs.SphereMesh(headGeo, bodyMat);


    var cannonGeo = new THREE.CylinderGeometry(5, 5, 60, 12);
    var cannon = new Physijs.CylinderMesh(cannonGeo, bodyMat);


    var mouth = new THREE.Object3D();

    mouth.position.y = 50;

    var wheelGeo = new THREE.CylinderGeometry(10, 10, 10, 10);
    var wheel1 = new Physijs.CylinderMesh(wheelGeo, bodyMat);
    var wheel2 = new Physijs.CylinderMesh(wheelGeo, bodyMat);
    var wheel3 = new Physijs.CylinderMesh(wheelGeo, bodyMat);
    var wheel4 = new Physijs.CylinderMesh(wheelGeo, bodyMat);
    var wheel5 = new Physijs.CylinderMesh(wheelGeo, bodyMat);
    var wheel6 = new Physijs.CylinderMesh(wheelGeo, bodyMat);

    wheel1.name = 'wheel1';
    wheel2.name = 'wheel2';
    wheel3.name = 'wheel3';
    wheel4.name = 'wheel4';
    wheel5.name = 'wheel5';
    wheel6.name = 'wheel6';

    var wheelgroup = new THREE.Group();
    wheelgroup.add(wheel1, wheel2, wheel3, wheel4, wheel5, wheel6);

    body.position.set(0, 30, -50);
    neck.position.set(0, 10, -5);
    head.position.set(0, 15, 0);
    cannon.position.set(0, 0, 45);
    cannon.rotation.set(degreesToRadians(90), 0, 0);

    wheel1.position.set(-35, -12, 25);
    wheel2.position.set(-35, -12, 0);
    wheel3.position.set(-35, -12, -25);
    wheel4.position.set(35, -12, 25);
    wheel5.position.set(35, -12, 0);
    wheel6.position.set(35, -12, -25);

    wheel1.rotation.set(0, 0, degreesToRadians(90));
    wheel2.rotation.set(0, 0, degreesToRadians(90));
    wheel3.rotation.set(0, 0, degreesToRadians(90));
    wheel4.rotation.set(0, 0, degreesToRadians(90));
    wheel5.rotation.set(0, 0, degreesToRadians(90));
    wheel6.rotation.set(0, 0, degreesToRadians(90));

    mouth.name = 'mouth';
    cannon.name = 'cannon';
    neck.name = 'neck';
    body.name = 'body';
    wheelgroup.name = 'wheelgroup';

    cannon.add(mouth);
    head.add(cannon);
    neck.add(head);
    body.add(neck);
    body.add(wheelgroup);
    body.castShadow = true;

    return body;
}

function addPlayer() {
    playerTank = buildTank(0);
    playerTank.position.set(0, 22, 0);
    playerTank.rotation.set(0, degreesToRadians(180), 0);

    scene.add(playerTank);

    // Scroll doesn't work on Firefox
    document.addEventListener('mousewheel', scroll, false);
    container.addEventListener('mousedown', mouseClick, false);  // Event for any mouse click
    document.addEventListener('keydown', pauseGame, false);  // Event to detect the player pausing the game
    document.addEventListener('keydown', listenForShoot, false);  // Event to detect the player shooting
    listenForTankRotation();
}

function addFog() {
    var fog = new THREE.FogExp2(0x999999, FOG_DENSITY);

    scene.fog = fog;
}

function addGround() {
    var groundMat = Physijs.createMaterial(new THREE.MeshPhongMaterial({ map: texture, side: THREE.FrontSide }), .8, .3);

    ground = new Physijs.HeightfieldMesh(new THREE.PlaneGeometry(MAP_WIDTH, MAP_HEIGHT, 50, 50), groundMat, 0);

    ground.rotation.x = degreesToRadians(-90);
    ground.name = 'ground';

    ground.receiveShadow = true;

    scene.add(ground);
}

function makeHole(holeX, holeZ) {
    var newGroundMat = ground.material;
    var newGroundGeo = new THREE.PlaneGeometry(MAP_WIDTH, MAP_HEIGHT, 50, 50);

    newGroundGeo.vertices = ground.geometry.vertices;

    for (var i = 0; i < newGroundGeo.vertices.length; i++) {
        var vertex = newGroundGeo.vertices[i];

        if (vertex.x <= holeX + 50 && vertex.x >= holeX - 50 && -vertex.y <= holeZ + 50 && -vertex.y >= holeZ - 50) {
            vertex.z = vertex.z - 5;
        }
    }

    newGround = new Physijs.HeightfieldMesh(newGroundGeo, newGroundMat, 0);

    newGround.rotation.x = degreesToRadians(-90);
    newGround.name = 'ground';

    scene.add(newGround);
    scene.remove(ground);

    ground = newGround;

    scene.updateMatrixWorld();
}

function addLights() {
    // Positions a light and places it in the scene
    var light = new THREE.DirectionalLight(0xffffff, 1.5);
    light.position.set(500, 550, -550);
    light.castShadow = true;

    light.shadow.camera.near = -1000;
    light.shadow.camera.far = 2000;
    light.shadow.mapSize.Width = 3000;
    light.shadow.mapSize.Height = 100;

    light.shadow.camera.top = 1000;
    light.shadow.camera.bottom = -1000;
    light.shadow.camera.left = 1000;
    light.shadow.camera.right = -1000;

    scene.add(light);
}

function addSun() {
    var geometry = new THREE.SphereGeometry(15, 20, 20);
    var material = Physijs.createMaterial(new THREE.MeshBasicMaterial({ color: 0xf4f142 }), .8, .3);
    var sphere = new Physijs.SphereMesh(geometry, material, 0);
    sphere.position.set(500, 250, -550);
    scene.add(sphere);

}

function addEnemyTank(addAnother, howMany, milliseconds) {
    if (numberEnemies < MAX_ENEMIES) {
        var posX = getRandomInt(SPAWN_DISTANCE);

        if (getRandomInt(1) == 0)
            posX = -posX;

        var posZ = Math.sqrt(Math.pow(SPAWN_DISTANCE, 2) - Math.pow(posX, 2));

        if (getRandomInt(1) == 0)
            posZ = -posZ;

        var tank = buildTank(100);

        tank.fire  = function () {
            if (this.enableFire) {
                this.enableFire = false;
                var neck = this.children[0];
                var head = neck.children[0];
                var cannon = head.children[0];
                var mouth = cannon.children[0];

                var spawnX = mouth.getWorldPosition().x;
                var spawnY = mouth.getWorldPosition().y;
                var spawnZ = mouth.getWorldPosition().z;

                var material = Physijs.createMaterial(new THREE.MeshPhongMaterial({ color: 'grey' }, .8, .3));
                var bulletGeo = new THREE.SphereGeometry(5, 10, 10);
                var bullet = new Physijs.SphereMesh(bulletGeo, material, 1);

                bullet.position.set(spawnX, spawnY, spawnZ);

                bullet.addEventListener('collision', function (other_object, linear_velocity, angular_velocity) {
                    if (other_object == playerTank) {
                        RemoveHP();
                        parts.push(
                            new ExplodeAnimation(playerTank.position.x, playerTank.position.z),
                            new ExplodeAnimation(playerTank.position.x-30, playerTank.position.z-30),
                            new ExplodeAnimation(playerTank.position.x-30, playerTank.position.z+30),
                            new ExplodeAnimation(playerTank.position.x+30, playerTank.position.z-30),
                            new ExplodeAnimation(playerTank.position.x+30, playerTank.position.z+30)
                        );
                    }
                    scene.remove(this);
                    setTimeout(function () { tank.enableFire = true; }, 5000);
                });

                bullet.name = "bullet";

                scene.add(bullet);

                power = 300;

                var cannonpos = cannon.getWorldPosition();
                var mouthpos = mouth.getWorldPosition();
                var direction = new THREE.Vector3();
                direction.subVectors(mouthpos, cannonpos).normalize();
                var impulse = new THREE.Vector3(direction.x * power, direction.y * power, direction.z * power);


                bullet.setLinearVelocity(impulse);

                // This is just so if any bullet's collision event fails, it gets erased later
                setTimeout(removeBullet, 5000, bullet);
            }
        };

        // I add the tank with some height so it doesn't spawn inside other possible tanks
        tank.position.set(posX, 50, posZ);
        tank.lookAt(playerTank.position);

        scene.add(tank);

        enemyTanks.push(tank);

        numberEnemies++;

        scene.updateMatrixWorld();
    }

    if (addAnother && howMany != undefined && milliseconds != undefined) {
        howMany = howMany - 1;

        if (howMany > 0)
            setTimeout(addEnemyTank, milliseconds, addAnother, howMany, milliseconds);
    }
}

function shoot(tank) {
    if (playerBullet == null) {
        var neck = tank.children[0];
        var head = neck.children[0];
        var cannon = head.children[0];
        var mouth = cannon.children[0];

        var spawnX = mouth.getWorldPosition().x;
        var spawnY = mouth.getWorldPosition().y;
        var spawnZ = mouth.getWorldPosition().z;

        var material = Physijs.createMaterial(new THREE.MeshPhongMaterial({ color: 'grey' }, .8, .3));
        var bulletGeo = new THREE.SphereGeometry(5, 10, 10);
        var bullet = new Physijs.SphereMesh(bulletGeo, material, 1);

        bullet.position.set(spawnX, spawnY, spawnZ);

        bullet.addEventListener('collision', function (other_object, linear_velocity, angular_velocity) {
            var tankInd = enemyTanks.indexOf(other_object);

            if (playerBullet == this) {
                playerBullet = null;
                
            }

            if (tankInd != -1) {
                scene.remove(other_object);
                enemyTanks.splice(tankInd, 1);
                dismantle(other_object.getWorldPosition());

                numberEnemies--;

                if (!pause && !gameOver) {
                    KillCounter++;
                }

                Kills.innerHTML = parseInt(KillCounter);

                addEnemyTank(true, 2, 2000);
            }
            else if (other_object.name == "ground") {
                // This can break the physics engine
                //makeHole(this.getWorldPosition().x, this.getWorldPosition().z);
            }

            scene.remove(this);
        });

        playerBullet = bullet;

        bullet.name = "bullet";
        bullet.castShadow = true;

        scene.add(bullet);

        power = SHOOT_POWER;

        var cannonpos = cannon.getWorldPosition();
        var mouthpos = mouth.getWorldPosition();
        var direction = new THREE.Vector3();
        direction.subVectors(mouthpos, cannonpos).normalize();
        var impulse = new THREE.Vector3(direction.x * power, direction.y * power, direction.z * power);

        bullet.setLinearVelocity(impulse);

        // This is just so if any bullet's collision event fails, it gets erased later
        setTimeout(removeBullet, 6000, bullet);
    }
}

// This is just so if any bullet's collision event fails, it gets erased later
function removeBullet(bullet) {
    if (scene.getObjectByName(bullet.name) != undefined) {
        if (playerBullet == bullet)
            playerBullet = null;

        scene.remove(bullet);
    }
}

function dismantle(pos) {
    var bodyMat = Physijs.createMaterial(new THREE.MeshPhongMaterial({ color: 'green' }, .8, .3));
    var body = new Physijs.BoxMesh(new THREE.BoxGeometry(70, 25, 90), bodyMat, 1);

    var neckGeo = new THREE.CylinderGeometry(25, 25, 30, 12);
    var neck = new Physijs.CylinderMesh(neckGeo, bodyMat, 1);

    var headGeo = new THREE.SphereGeometry(25, 12, 12);
    var head = new Physijs.SphereMesh(headGeo, bodyMat, 1);

    var cannonGeo = new THREE.CylinderGeometry(5, 5, 60, 12);
    var cannon = new Physijs.CylinderMesh(cannonGeo, bodyMat, 1);

    var wheelGeo = new THREE.CylinderGeometry(10, 10, 10, 10);
    var wheel1 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);
    var wheel2 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);
    var wheel3 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);
    var wheel4 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);
    var wheel5 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);
    var wheel6 = new Physijs.CylinderMesh(wheelGeo, bodyMat, 1);

    var x = pos.x;
    var y = pos.y;
    var z = pos.z;

    body.position.set(x, y, z);
    neck.position.set(x, y, z);
    head.position.set(x, y, z);
    cannon.position.set(x, y, z);

    wheel1.position.set(x, y, z);
    wheel2.position.set(x, y, z);
    wheel3.position.set(x, y, z);
    wheel4.position.set(x, y, z);
    wheel5.position.set(x, y, z);
    wheel6.position.set(x, y, z);

    body.castShadow = true;
    head.castShadow = true;
    neck.castShadow = true;
    cannon.castShadow = true;
    wheel1.castShadow = true;
    wheel2.castShadow = true;
    wheel3.castShadow = true;
    wheel4.castShadow = true;
    wheel5.castShadow = true;
    wheel6.castShadow = true;

    scene.add(body);
    scene.add(neck);
    scene.add(head);
    scene.add(cannon);
    scene.add(wheel1);
    scene.add(wheel2);
    scene.add(wheel3);
    scene.add(wheel4);
    scene.add(wheel5);
    scene.add(wheel6);

    if (dismantledParts.length > 5) {
        for (var i = 0; i < dismantledParts.length; i++) {
            scene.remove(dismantledParts[i]);
        }
        dismantledParts.splice(0, dismantledParts.length);
    }

    dismantledParts.push(body, head, neck, cannon, wheel1, wheel2, wheel3, wheel4, wheel5, wheel6);

    parts.push(
        new ExplodeAnimation(pos.x, pos.z),
        new ExplodeAnimation(pos.x + 20, pos.z + 20),
        new ExplodeAnimation(pos.x - 20, pos.z + 20),
        new ExplodeAnimation(pos.x - 20, pos.z - 20),
        new ExplodeAnimation(pos.x + 20, pos.z - 20),
        new ExplodeAnimation(pos.x + 50, pos.z - 50),
        new ExplodeAnimation(pos.x + 50, pos.z - 50),
        new ExplodeAnimation(pos.x + 50, pos.z - 50),
        new ExplodeAnimation(pos.x + 50, pos.z - 50)
    );

    scene.updateMatrixWorld();
}

function ExplodeAnimation(x, z) {
    var geometry = new THREE.Geometry();

    for (i = 0; i < totalParts; i++) {
        var vertex = new THREE.Vector3();
        vertex.x = x;
        vertex.z = z;
        vertex.y = 1;

        geometry.vertices.push(vertex);
        dirs.push({ x: (Math.random() * partsSpeed) - (partsSpeed / 2), y: (Math.random() * partsSpeed) - (partsSpeed / 2), z: (Math.random() * partsSpeed) - (partsSpeed / 2) });
    }
    var material = new THREE.ParticleBasicMaterial({ size: partsSize, color: partsColors[Math.round(Math.random() * partsColors.length)] });
    var particles = new THREE.ParticleSystem(geometry, material);

    this.object = particles;
    this.status = true;

    this.xDir = (Math.random() * partsSpeed) - (partsSpeed / 2);
    this.yDir = (Math.random() * partsSpeed) - (partsSpeed / 2);
    this.zDir = (Math.random() * partsSpeed) - (partsSpeed / 2);

    scene.add(this.object);

    this.update = function () {
        if (this.status == true) {
            var pCount = totalParts;
            while (pCount--) {
                var particle = this.object.geometry.vertices[pCount]
                particle.y += dirs[pCount].y;
                particle.x += dirs[pCount].x;
                particle.z += dirs[pCount].z;
            }
            this.object.geometry.verticesNeedUpdate = true;
        }
    }

}

function animate() {

    if (!pause && !menuON) {

        // For some reason, the physics engine started malfunctioning after we did some changes that we're unaware of
        // The physics engine detects the colisions, but the events aren't fired
        // If we simulate the scene with console.log(scene.simulate()); instead of scene.simulate(); it works most of the times
        // No idea why

        //scene.simulate();
        
        console.log(scene.simulate());
        TanksLookAtPlayer();
        updatePlayerRotation();
    }

    render();

    requestAnimationFrame(animate); // Updates the render continuously
}

function TanksLookAtPlayer() {

    for (i = 0; i < enemyTanks.length; i++) {

        if (Math.sqrt(Math.pow(enemyTanks[i].position.x, 2) + Math.pow(enemyTanks[i].position.z, 2)) > MIN_DISTANCE) {
            var speed = 50;

            var tankpos = enemyTanks[i].getWorldPosition();
            var playerpos = playerTank.getWorldPosition();
            var direction = new THREE.Vector3();
            direction.subVectors(playerpos, tankpos).normalize();
            var movement = new THREE.Vector3(direction.x * speed, direction.y * speed, direction.z * speed);

            enemyTanks[i].setLinearVelocity(movement);
        }
        else {
            enemyTanks[i].setLinearVelocity(new THREE.Vector3(0, 0, 0));
            if(enemyTanks[i].enableFire==undefined)
                enemyTanks[i].enableFire = true;
            enemyTanks[i].fire();
        }
        enemyTanks[i].lookAt(playerTank.position);
        enemyTanks[i].__dirtyRotation = true;
    }
}

function render() {
    var pCount = parts.length;
    while (pCount--) {
        parts[pCount].update();
    }

    renderer.render(scene, camera); // Renders the scene
}

// pause game function
function pauseGame() {

    if (event.keyCode == 80)    // 'P' key
    {
        // The game will be paused if it isn't already paused and the player isn't on a menu
        if (pause == false && menuON == false) {
            pause = true;

            messagePause.style.display = '';
            controls.enabled = false;
            document.exitPointerLock();
        }
        else {
            pause = false;

            messagePause.style.display = "none";

            // If the player isn't on a menu, the controls will be activated
            if (menuON == false) {
                scene.onSimulationResume();
                controls.enabled = true;
                container.requestPointerLock();
            }

        }
    }
}

// Resizes the canvas to match the window
function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function radiansToDegrees(radians) {
    return radians * 180 / Math.PI;
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max + 1));
}

// increments the clock
function setTime() {
    // It will only increment if the game hasn't finished and isn't paused
    if (!pause && !gameOver) {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds % 60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
 
    }
}
// add padding to clock numbers
function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}

function scroll(event) {
    if (!pause && !menuON) {
        if (controls.enabled == true) {
            if (event.wheelDelta >= 0) {
                if (Math.sqrt(Math.pow(camera.position.x, 2) + Math.pow(camera.position.z, 2)) >= 150)
                    camera.translateZ(-10);
            }
            else {
                if (Math.sqrt(Math.pow(camera.position.x, 2) + Math.pow(camera.position.z, 2)) <= 750)
                    camera.translateZ(10);
            }
        }
    }
}

function mouseClick(event) {
    // A player can only shoot if the game isn't paused
    if (!pause && !menuON) {
        if (controls.enabled == false) {
            container.requestPointerLock();
            return;
        }
        else {
            shoot(playerTank);
        }
    }
}

function menuSelection() {

    // Button start
    buttons.children[0].onclick = function () {
        menu.style.display = "none";
        menuON = false;
        stats.style.display = '';

        //hp.style.display = '';

        setInterval(setTime, 1000);
        container.requestPointerLock(); // Locks the mouse controls to the canvas
        crosshair.style.display = '';
    }

    document.addEventListener('pointerlockchange', lockChange, false);  // To enable and disable the controls
}

function lockChange() {
    if (document.pointerLockElement === container)
        controls.enabled = true;
    else
        controls.enabled = false;
}

function updatePlayerRotation() {
    if (xisrotatingup && playerTank.children[0].children[0].rotation.x - xrotation > degreesToRadians(-45))
        playerTank.children[0].children[0].rotation.x -= xrotation;
    else if (xisrotatingdown && playerTank.children[0].children[0].rotation.x + xrotation < degreesToRadians(10))
        playerTank.children[0].children[0].rotation.x += xrotation;

    if (yisrotatingleft)
        playerTank.children[0].rotation.y += yrotation;
    else if (yisrotatingright)
        playerTank.children[0].rotation.y -= yrotation;
}

function listenForShoot() {

    if (event.keyCode == 32)    // Space key
    {
        if (!pause && !menuON) {

            if (controls.enabled == false) {
                container.requestPointerLock();
                return;
            }
            else {
                shoot(playerTank);
            }
        }
    }
}

function listenForTankRotation() {

    // A key has been pressed
    var onKeyDown = function (event) {

        if (controls.enabled == true) {
            switch (event.keyCode) {

                case 38: // up
                case 87: // w
                    xisrotatingup = true;
                    break;

                case 37: // left
                case 65: // a
                    yisrotatingleft = true;
                    break;

                case 40: // down
                case 83: // s
                    xisrotatingdown = true;
                    break;

                case 39: // right
                case 68: // d
                    yisrotatingright = true;
                    break;
            }
            scene.updateMatrixWorld();
        }
    };

    // A key has been released
    var onKeyUp = function (event) {

        switch (event.keyCode) {

            case 38: // up
            case 87: // w
                xisrotatingup = false;
                break;

            case 37: // left
            case 65: // a
                yisrotatingleft = false;
                break;

            case 40: // down
            case 83: // s
                xisrotatingdown = false;
                break;

            case 39: // right
            case 68: // d
                yisrotatingright = false;
                break;
        }
    };

    // Add event listeners for when movement keys are pressed and released
    document.addEventListener('keydown', onKeyDown, false);
    document.addEventListener('keyup', onKeyUp, false);
}

function RemoveHP() {

    hpCounter--;

    switch (hpCounter) {

        case 1:
            hp1.style.display = "none";

            scene.remove(playerTank);
            dismantle(playerTank.getWorldPosition());

            // Game over
            gameOver = true;
            messageGameOver.style.display = ''; // Shows the game over text
            controls.enabled = false;
            document.exitPointerLock();

            break;
        case 2:
            hp2.style.display = "none";
            break;
        case 3:
            hp3.style.display = "none";
            break;
        default:
            break;
    }

}

