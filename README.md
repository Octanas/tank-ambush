# Tank Ambush

3rd person wave survival game made with Three.JS and the Physijs library. The player controls a stationary tank that has to defend itself from incoming enemy tanks, which come in waves and will try to get close to the player to destroy him. The player will be able to control the cannon's yaw and pitch and the power of the shot.

## Notes
- Some blocks of code are based on [this tutorial](https://docs.microsoft.com/en-us/windows/uwp/get-started/get-started-tutorial-game-js3d).
- Even though this project was built on a UWP application, the use of a browser to run the game is advisable, as the UWP app suffers from ["movement lag and unregistered keyUp events"](https://docs.microsoft.com/en-us/windows/uwp/get-started/get-started-tutorial-game-js3d#5-adding-player-movement).
